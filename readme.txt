﻿# Đồ án mẫu thiết kế và ứng dụng
# Nội dung: xem file .pdf
# Ngôn ngữ: C# (winform)
# IDE: Visual Studio 2017
# Hướng dẫn:
	- clone source code từ bitbutket về máy (sử dụng git cmd hoặc sourcetree).
	- checkout vào branch develop.
	- pull code về.
# Chú ý: 
	- Để tránh conflig khi merge, mọi người tạo branch và checkout về branch của mình rồi mới bắt đầu viết.
	- Tạo pull request để yêu cầu merge code, không tự ý merge.
	- Nên sử dụng sourcetree để quản lý code dễ dàng.
	- Cần tìm hiểu về mẫu Dependence Injection và xem lại kiến thức winform.