var db = require('../fn/mysql');
const fillterDB = ['mysql', '_database', 'phpmyadmin', 
  'infomation_schema', 'performance_schema'];

export function createNewDatabase(dbName) {
  if (dbName && dbName !== "" && fillterDB.indexOf(dbName) === -1) {
    let sql = `CREATE DATABASE IF NOT EXISTS ${dbName} CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci'`;
    return db.executedSQL(sql, 'mysql');
  } 
  return 400;
}

export async function createNewTable(dbName, table) {
  let sql = `CREATE TABLE IF NOT EXISTS ${table} (__id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT)`;
  return await db.executedSQL(sql, dbName);
}

export async function createDataTable(obj) {
  await createNewDatabase(obj.database);
  let sqls = generateSQLS(obj);
  for (let i = 0; i < sqls.length; i++) {
    console.log(sqls[i]);
    await db.executedSQL(sqls[i], obj.database);
  }
  return {"status" : 200};
}

/*
let js = {
  "database":"db",
  "b1":{"c11":"True","c12":"True","c13":"True"},
  "b2":{"c21":"True"},"b3":{"c31":"True","c32":"True","c33":"True"}
};
*/

function generateSQLS(obj) {
  let columns = [];
  let sqls = [];
  for (let key in obj) {
    if (key !== "database") {
      for (let _key in obj[key]) {
        columns.push(_key);
      }
      sqls.push(`CREATE TABLE IF NOT EXISTS ${key}(__id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, ${columns.join(" VARCHAR(20),")} VARCHAR(20)) ENGINE = InnoDB`);
      columns = [];
    }
  }
  return sqls;
}