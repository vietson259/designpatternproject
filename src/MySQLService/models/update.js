var db = require('../fn/mysql');

const fillterDB = ['mysql', '_database', 'phpmyadmin', 
  'infomation_schema', 'performance_schema'];

export function addNewColumn(dbName, table, column) {
  if (dbName && dbName !== "") {
    let sql = `ALTER TABLE ${table} ADD COLUMN ${column}`;
    return db.executedSQL(sql, dbName);
  } 
  return 400;
}

export async function addRow(obj) {
  console.log(obj);
  let dbName = obj.database;
  let table = obj.table;
  let arrValues = [];
  let arrFields = [];
  for (let key in obj) {
    if (key !== "database" && key !== "table") {
      arrFields.push(key);
      arrValues.push(obj[key]);
    }
  }
  if (1) {
    let sql = `INSERT INTO ${table} (${arrFields.join(",")}) VALUES ('${arrValues.join("','")}')`;
    console.log(sql);
    return db.executedSQL(sql, dbName);
  } 
  return 400;
}

export function updateRow(jsonRow) {
  jsonRow = JSON.parse(jsonRow);
  let dbName = jsonRow.database;
  let arrFields = [];
  for (let key in jsonRow.data) {
    arrFields.push(`${key}=${jsonRow.data[key]}`);
  }
  if (dbName && dbName !== "" && fillterDB.indexOf(dbName) === -1) {
    let sql = `UPDATE ${jsonRow.table} SET ${arrFields.join(",")}`;
    return db.executedSQL(sql, jsonRow.database);
  } 
  return 400;
}
