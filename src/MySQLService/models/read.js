var db = require('../fn/mysql');
const filterDB = ['mysql', 'phpmyadmin', 
  'infomation_schema', 'performance_schema'];

export function getDataTable(dbName, table) {
  if (dbName && dbName !== "" && table && table !== "") {
    let sql = `select * from ${table}`;
    return db.executedSQL(sql, dbName);
  } 
  return 400;
}

export function getAllTablesInDb(dbName) {
  if (dbName && dbName !== "") {
    let sql = `SELECT table_name FROM information_schema.tables where table_schema='${dbName}'`;
    return db.executedSQL(sql, dbName);
  } 
  return 400;
}

export function getAllDbName() {
  try {
    let sql = `SELECT TABLE_SCHEMA FROM information_schema.tables GROUP BY TABLE_SCHEMA`;
    return db.executedSQL(sql, 'mysql');
  } catch (e) {
    res.json({"status": 400});
  }
  return 400;
}

export function demo() {
  try {
    let sql = `SELECT StudentID, StudentName, DOB, ClassName from Student, MClass`;
    return db.executedSQL(sql, 'demo2');
  } catch (e) {
    res.json({"status": 400});
  }
  return 400;
}