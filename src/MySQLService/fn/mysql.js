import mysql from "mysql";

export function connectDB(dbName) {
  return mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: dbName
  });
}

export function executedSQL(sql, dbName) {
  return new Promise((resolve, reject) => {
    let cn = connectDB(dbName);
    cn.connect();
    cn.query(sql, function (error, rows, fields) {
      if (error) {
        reject(error);
      } else {
        resolve(rows);
      }
      cn.end();
    });
  });
}