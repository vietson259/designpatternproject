import express from 'express';
import path from 'path';

var bodyParser = require('body-parser');
var dbCreated = require('./models/create');
const PORT = process.env.PORT || 5000

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.listen(PORT, async () => {
  //await dbCreated.configDatabase();
  console.log(`Site running on port ${PORT}`);
});

app.get("/", (req, res) => {
  res.json({"status": 200});
});

var createController = require("./controllers/createController");
var deleteController = require("./controllers/deleteController");
var updateController = require("./controllers/updateController");
var readController = require("./controllers/readController");
app.use('/create', createController);
app.use('/delete', deleteController);
app.use('/update', updateController);
app.use('/read', readController);
app.use('/demo', readController);
