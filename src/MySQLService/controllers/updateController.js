var express = require('express');
var router = express.Router();

var dbUpdate = require('../models/update');
router.post('/updateNewColumn', async (req, res) => {
  try {
    if (req.body.dbName && req.body.dbName !== "") {
      res.json(await dbUpdate.addNewColumn(req.body.dbName, req.body.table, req.body.column));
    } else {
      res.json({"error": "name not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/insertRow', async (req, res) => {
  try {
    if (req.body && req.body !== "") {
      console.log(req.body);
      res.json(await dbUpdate.addRow(req.body));
    } else {
      res.json({"error": "name not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});
module.exports = router;