var express = require('express');
var router = express.Router();

var dbCreated = require('../models/create');
router.post('/createNewDatabase', async (req, res) => {
  try {
    if (req.body) {
      let dbName = req.body.database;
      dbCreated.createNewDatabase(dbName).then(res => console.log(res));
      for (let key in req.body) {
        if (key !== "database")
          dbCreated.createNewTable(dbName, req.body[key]).then(res => console.log(res));
      }
      res.json({"status": 200});
    } else {
      res.json({"error": "body not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/createNewTable', async (req, res) => {  
  try {
    if (req.body) {
      let _res = await dbCreated.createNewTable(req.body.content);
      res.json({"status": _res});
    } else {
      res.json({"error": "name not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/createDataTable', async (req, res) => {  
  try {
    if (req.body) {
      res.json(await dbCreated.createDataTable(req.body));
    } else {
      res.json({"error": "name not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});
module.exports = router;