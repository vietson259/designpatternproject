var express = require('express');
var router = express.Router();

var dbRead = require('../models/read');
router.get('/getAllDb', async (req, res) => {
  try {
    res.json(await dbRead.getAllDbName());
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/getAllTablesInDb', async (req, res) => {
  try {
    if (req.body.content && req.body.content !== "") {
      res.json(await dbRead.getAllTablesInDb(req.body.content));
    } else {
      res.json({"error": "dbName not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/getDataTable', async (req, res) => {
  try {
    if (req.body.dbName && req.body.dbName !== "" && req.body.table && req.body.table !== "") {
      res.json(await dbRead.getDataTable(req.body.dbName, req.body.table));
    } else {
      res.json({"error": "dbName not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/demo', async (req, res) => {
  try {
    if (req.body) {
      res.json(await dbRead.demo(req.body));
    } else {
      res.json({"error": "dbName not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

module.exports = router;