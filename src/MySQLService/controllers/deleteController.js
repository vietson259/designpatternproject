var express = require('express');
var router = express.Router();

var dbDeleted = require('../models/delete');
router.post('/deleteDatabase', async (req, res) => {
  try {
    if (req.body.dbName && req.body.dbName !== "") {
      res.json(await dbDeleted.deletedDatabase(req.body.dbName));
    } else {
      res.json({"error": "dbName not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/deleteTable', async (req, res) => {
  try {
    if (req.body.dbName && req.body.tableName && req.body.dbName !== "" && req.body.tableName !== "") {
      res.json(await dbDeleted.deletedTable(req.body.dbName, req.body.tableName));
    } else {
      res.json({"error": "table or db name not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

router.post('/deleteRow', async (req, res) => {
  try {
    if (req.body) {
      res.json(await dbDeleted.deletedRow(req.body));
    } else {
      res.json({"error": "table or db name not found"});
    }
  } catch (error) {
    res.json({"error": "400"});
  }
});

module.exports = router;