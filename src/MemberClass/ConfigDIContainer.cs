﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_30_18_POOP
{
    public static class ConfigDIContainer
    {
        public static void Config()
        {
            DIContainer.SetModule<IMembership, Server>();
            DIContainer.SetModule<Client, Client>();
        }
    }
}
