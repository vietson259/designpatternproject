﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberClass
{
    public class CreatedEventAction : Form
    {
        public CreatedEventAction(IMembership membership) : base(membership)
        {

        }

        public override void DoAction()
        {
            _membership.ICreated();
        }
    }
}
