﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberClass
{
    public class DeletedEventAction : Form
    {
        public DeletedEventAction(IMembership membership) : base(membership)
        {

        }

        public override void DoAction()
        {
            _membership.IDeleted();
        }
    }
}