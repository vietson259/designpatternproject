﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberClass
{
    public abstract class MembershipSQL : IMembership
    {

        public abstract void IConnect();

        public abstract object ILoadData();

        public abstract void ICreated();

        public abstract void IDeleted();

        public abstract void IRead();

        public abstract void IUpdated();
    }
}
