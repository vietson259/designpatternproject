﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberClass
{
    public abstract class Form
    {
        protected List<string> _data;
        protected readonly IMembership _membership;
        public Form(IMembership membership)
        {
            _membership = membership;
            _membership.IConnect();
            _data = (List<string>)_membership.ILoadData();
        }
        public abstract void DoAction();
        public void Display()
        {
            Console.WriteLine(_data.ToString());
        }
    }
}
