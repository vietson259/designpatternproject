﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberClass
{
    public class MembershipMSSQL : MembershipSQL
    {
        public override void IConnect()
        {
            Console.WriteLine("connect database by mssql");
        }

        public override void ICreated()
        {
            Console.WriteLine("created new database by mssql");
        }

        public override void IDeleted()
        {
            Console.WriteLine("deleted database by mssql");
        }

        public override object ILoadData()
        {
            Console.WriteLine("load data from database by mssql");
            return new List<string>()
            {
                "1mssql", "2mssql", "3mssql"
            };
        }

        public override void IRead()
        {
            Console.WriteLine("read data by mssql");
        }

        public override void IUpdated()
        {
            Console.WriteLine("updated data by mssql");
        }
    }
}