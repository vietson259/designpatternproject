﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberClass
{
    public class JSONObjectArray : JSONObject
    {
        List<JSONObject> ListJSONObjects = new List<JSONObject>();
        public JSONObjectArray(string jsonString)
        {
            jsonString = ClearCharacter(jsonString, '[');
            jsonString = ClearCharacter(jsonString, ']');
            List<string> jsonStringArray = SplitJsonString(jsonString);
            foreach (string e in jsonStringArray)
            {
                ListJSONObjects.Add(new JSONObject(e));
            }
        }
        private List<string> SplitJsonString(string jsonString)
        {
            jsonString = ClearCharacter(jsonString, ' ');
            jsonString = ClearCharacter(jsonString, '\n');
            List<string> list = new List<string>();
            string temp = "";
            for (int i = 0, length = jsonString.Length; i < length; i++)
            {
                if (i > 0 && jsonString[i] == ',' && jsonString[i - 1] == '}')
                {
                    list.Add(temp);
                    temp = "";
                } else
                {
                    temp += jsonString[i];
                }
            }
            return list;
        }
        public override string ToJSON()
        {
            string res = "[";
            for (int i = 0, length = ListJSONObjects.Count(); i < length - 1; i++)
            {
                res += (ListJSONObjects[i].ToJSON() + ",");
            }
            res += ListJSONObjects[ListJSONObjects.Count() - 1].ToJSON();
            return res + "]";
        }
        public override JSONSimple ToJSONSimple()
        {
            JSONSimple jSONSimple = new JSONSimple();
            List<List<string>> values = new List<List<string>>();
            List<string> fields = new List<string>();
            for (int i = 0, length = ListJSONObjects.Count(); i < length; i++)
            {
                List<string> value = new List<string>();
                foreach (var e in ListJSONObjects[i].Data)
                {
                    if (i == 0)
                    {
                        fields.Add(e.Key);
                        value.Add(e.Value);
                    } else
                    {
                        value.Add(e.Value);
                    }
                }
                values.Add(value);
            }
            jSONSimple.Fields = fields;
            jSONSimple.Values = values;
            return jSONSimple;
        }
    }
}
