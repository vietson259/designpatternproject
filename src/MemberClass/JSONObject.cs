﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberClass
{
    public class JSONObject
    {
        public JSONObject() { }
        public Dictionary<string, string> Data = new Dictionary<string, string>();
        public JSONObject(string jsonString)
        {
            jsonString = ClearCharacter(jsonString, '{');
            jsonString = ClearCharacter(jsonString, '}');
            jsonString = ClearCharacter(jsonString, '"');
            jsonString = ClearCharacter(jsonString, '\n');
            string[] pairValues = jsonString.Split(',');
            Data.Clear();
            foreach (string e in pairValues)
            {
                string[] tempArray = e.Split(':');
                Data.Add(tempArray[0].Trim(), tempArray[1].Trim());
            }
        }
        public virtual string ToJSON()
        {
            List<string> list = new List<string>();
            foreach (var e in Data)
            {
                string temp = "\"" + e.Key + "\"" + ":" + "\"" + e.Value + "\"";
                list.Add(temp);
            }
            return "{" + ArrayJoin(list, ',') + "}";
        }
        protected string ArrayJoin(List<string> list, char c)
        {
            string res = "";
            for (int i = 0, length = list.Count(); i < length - 1; i++)
            {
                res += (list[i] + c);
            }
            res += list[list.Count() - 1];
            return res;
        }
        protected string ClearCharacter(string src, char c)
        {
            string res = "";
            for (int i = 0; i < src.Length; i++)
            {
                if (src[i] == c) continue;
                res += src[i];
            }
            return res;
        }
        public virtual JSONSimple ToJSONSimple()
        {
            JSONSimple jSONSimple = new JSONSimple();
            List<string> value = new List<string>();
            List<List<string>> values = new List<List<string>>();
            List<string> fields = new List<string>();
            foreach (var e in Data)
            {
                fields.Add(e.Key);
                value.Add(e.Value);
            }
            values.Add(value);
            jSONSimple.Values = values;
            jSONSimple.Fields = fields;
            return jSONSimple;
        }
    }
}
