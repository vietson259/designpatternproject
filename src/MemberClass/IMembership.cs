﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberClass
{
    public interface IMembership
    {
        void IConnect();
        void ICreated();
        void IRead();
        void IUpdated();
        void IDeleted();
        object ILoadData();
    }
}