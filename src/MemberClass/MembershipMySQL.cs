﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace MemberClass
{
    public class MembershipMySQL : MembershipSQL
    {
        public override void IConnect()
        {
            Console.WriteLine("connect to mysql");
            string connetionString = null;
            connetionString = "server=localhost;database=world;port=8089;uid=root;pwd=1234;";
            MySqlConnection cnn = new MySqlConnection(connetionString);
            try
            {
                cnn.Open();
                MessageBox.Show("Connection Open ! ");
                try
                {
                    string sql = "SHOW DATABASES";
                    MySqlCommand cmd = new MySqlCommand(sql, cnn);
                    MySqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Console.WriteLine(rdr[0] + " -- " + rdr[1]);
                    }
                    rdr.Close();
                }
                catch (Exception ex1)
                {
                    Console.WriteLine(ex1.ToString());
                }
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! ");
            }
        }

        public override void ICreated()
        {
            Console.WriteLine("created new database by mysql");
        }

        public override void IDeleted()
        {
            Console.WriteLine("deteled database by mysql");
        }

        public override object ILoadData()
        {
            Console.WriteLine("load data from database by mysql");
            return new List<string>()
            {
                "1mysql", "2mysql", "3mysql"
            };
        }

        public override void IRead()
        {
            Console.WriteLine("read data by mysql");
        }

        public override void IUpdated()
        {
            Console.WriteLine("updated data by mysql");
        }
    }
}