﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    public class JSONSimple
    {
        private List<string> _fields;
        private List<List<string>> _values;
        public List<string> Fields { get => _fields; set => _fields = value; }
        public List<List<string>> Values { get => _values; set => _values = value; }
    }
}
