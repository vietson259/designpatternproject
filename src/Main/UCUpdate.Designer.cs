﻿namespace Main
{
    partial class UCUpdate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_UCUpdate = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.bntNewCol = new System.Windows.Forms.Button();
            this.btnDeleteRow = new System.Windows.Forms.Button();
            this.btnDeleteColumn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UCUpdate)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_UCUpdate
            // 
            this.dataGridView_UCUpdate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_UCUpdate.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_UCUpdate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_UCUpdate.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_UCUpdate.Name = "dataGridView_UCUpdate";
            this.dataGridView_UCUpdate.Size = new System.Drawing.Size(549, 267);
            this.dataGridView_UCUpdate.TabIndex = 0;
            this.dataGridView_UCUpdate.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_UCUpdate_CellClick);
            this.dataGridView_UCUpdate.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_UCUpdate_CellValueChanged);
            this.dataGridView_UCUpdate.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_UCUpdate_ColumnHeaderMouseDoubleClick);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(411, 277);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(130, 39);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save Change";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // bntNewCol
            // 
            this.bntNewCol.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntNewCol.Location = new System.Drawing.Point(17, 277);
            this.bntNewCol.Name = "bntNewCol";
            this.bntNewCol.Size = new System.Drawing.Size(130, 39);
            this.bntNewCol.TabIndex = 2;
            this.bntNewCol.Text = "New Column";
            this.bntNewCol.UseVisualStyleBackColor = true;
            this.bntNewCol.Click += new System.EventHandler(this.bntNewCol_Click);
            // 
            // btnDeleteRow
            // 
            this.btnDeleteRow.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteRow.Location = new System.Drawing.Point(153, 277);
            this.btnDeleteRow.Name = "btnDeleteRow";
            this.btnDeleteRow.Size = new System.Drawing.Size(116, 39);
            this.btnDeleteRow.TabIndex = 3;
            this.btnDeleteRow.Text = "Delete Row";
            this.btnDeleteRow.UseVisualStyleBackColor = true;
            this.btnDeleteRow.Click += new System.EventHandler(this.btnDeleteRow_Click);
            // 
            // btnDeleteColumn
            // 
            this.btnDeleteColumn.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteColumn.Location = new System.Drawing.Point(275, 277);
            this.btnDeleteColumn.Name = "btnDeleteColumn";
            this.btnDeleteColumn.Size = new System.Drawing.Size(130, 39);
            this.btnDeleteColumn.TabIndex = 4;
            this.btnDeleteColumn.Text = "Delete Column";
            this.btnDeleteColumn.UseVisualStyleBackColor = true;
            this.btnDeleteColumn.Click += new System.EventHandler(this.btnDeleteColumn_Click);
            // 
            // UCUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDeleteColumn);
            this.Controls.Add(this.btnDeleteRow);
            this.Controls.Add(this.bntNewCol);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dataGridView_UCUpdate);
            this.Name = "UCUpdate";
            this.Size = new System.Drawing.Size(549, 327);
            this.Load += new System.EventHandler(this.UCUpdate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UCUpdate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_UCUpdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button bntNewCol;
        private System.Windows.Forms.Button btnDeleteRow;
        private System.Windows.Forms.Button btnDeleteColumn;
    }
}
