﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Main
{
    public partial class FormBase : Form
    {
        protected readonly IMembership _membership;
        //Bang duoc chon de doc
        protected string ChooseTable = null;
        //Db duoc chon, chua bang de doc
        protected string ChooseDb = "mysql";
        protected string ChooseTableDelete = null;
        protected List<List<string>> dataToShow = null;
        protected List<string> column = null;
        protected JSONObjectArray jsonObjectArray = null;
        protected string updateState = "";

        protected List<string> listDb = new List<string>();
        Dictionary<string, List<string>> listTableName = new Dictionary<string, List<string>>();

        public FormBase(IMembership membership)
        {
            InitializeComponent();
            _membership = membership;
        }

        private void FormBase_Load(object sender, EventArgs e)
        {
            string res = _membership.IConnect(ChooseDb);
            JSONObject resJSON = new JSONObject(res);
            resJSON.Data.TryGetValue("status", out res);
            if (res == "200") MessageBox.Show("Connect DB success");
            else
            {
                MessageBox.Show("Connect DB unsuccess");
                return;
            }

            ReadListDb_TableName();

            if (listDb != null && listTableName != null)
            {
                ucTableName.NodeClick += new TreeNodeMouseClickEventHandler(UCTableName_NodeTreeViewClick);
                ucTableName.NodeDoubleClick += new TreeNodeMouseClickEventHandler(UCTableName_NodeTreeViewDoubleClick);

                SetUCTableName();

                ucTableName.Location = new Point(0, 50);

                this.panelListTableName.Controls.Add(ucTableName);
            }

        }

        private void ReadListDb_TableName()
        {
            jsonObjectArray = new JSONObjectArray(_membership.ILoadDBName());
            listDb.Clear();
            listTableName.Clear();

            //Danh sach ten cac db
            foreach (List<string> element in jsonObjectArray.ToJSONSimple().Values)
            {
                listDb.Add(element[0]);
            }

            //Danh sach table trong moi db
            foreach (string dbName in listDb)
            {
                List<string> listTables = new List<string>();
                jsonObjectArray = new JSONObjectArray(_membership.ILoadTableName(dbName));
                foreach (List<string> element in jsonObjectArray.ToJSONSimple().Values)
                {
                    listTables.Add(element[0]);
                }
                listTableName.Add(dbName, listTables);
            }
        }

        private void SetUCTableName()
        {
            foreach (string s in listDb)
            {
                TreeNode n = new TreeNode(s);
                foreach (var x in listTableName[s])
                {
                    n.Nodes.Add(new TreeNode(x));
                }

                ucTableName.AddListDb(n);
            }
        }

        private void UCTableName_NodeTreeViewClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Level != 0)
            {
                ChooseTable = e.Node.Text;
                ChooseDb = ((TreeView)sender).TopNode.Text;
            }
        }

        private void UCTableName_NodeTreeViewDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Level != 0)
            {
                if (!this.panelUserControl.Contains(ucRead))
                {
                    this.panelUserControl.Controls.Clear();
                    this.panelUserControl.Controls.Add(ucRead);
                }
                ChooseDb = e.Node.Parent.Text;
                ChooseTable = e.Node.Text;
                ChooseTableDelete = e.Node.Text;

                SetLabelTableName(ChooseTable);

                //Set lai schema

                jsonObjectArray = new JSONObjectArray(_membership.IRead(ChooseDb ,ChooseTable));

                this.panelUserControl.Controls.Add(ucRead);
                column = (jsonObjectArray.ToJSONSimple()).Fields;
                dataToShow = (jsonObjectArray.ToJSONSimple()).Values;

                this.ucRead.ShowData(column, dataToShow);
            }
        }

        private void UCUpdate_ButtonSaveClick(object sender, EventArgs e)
        {
            //if (updateState == "newColumn")
            //{
            //    _membership.IUpdateNewColumn(ChooseDb, ChooseTable, )
            //}
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            if (ChooseTable != null)
            {
                if (!this.panelUserControl.Contains(ucRead))
                {
                    this.panelUserControl.Controls.Clear();
                    this.panelUserControl.Controls.Add(ucRead);                    
                }
                ChooseTableDelete = ChooseTable;
                SetLabelTableName(ChooseTable);

                //Set lai schema dung bien ChooseDb

                jsonObjectArray = new JSONObjectArray(_membership.IRead(ChooseDb ,ChooseTable));

                this.panelUserControl.Controls.Add(ucRead);
                column = (jsonObjectArray.ToJSONSimple()).Fields;
                dataToShow = (jsonObjectArray.ToJSONSimple()).Values;

                this.ucRead.ShowData(column, dataToShow);
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (!this.panelUserControl.Controls.Contains(ucCreate))
            {
                SetLabelTableName("Create New Table In Database");
                this.panelUserControl.Controls.Clear();
                this.ucCreate.UCCreate_DoneClick += UCCreate_ButtonDoneClick;
                this.panelUserControl.Controls.Add(ucCreate);
            }
        }

        private void UCCreate_ButtonDoneClick(object sender, EventArgs e)
        {
            Ajax.POST(API.createData, ucCreate.DataToSave);

            //Update UCTableName
            ucTableName.Clear();
            ReadListDb_TableName();
            SetUCTableName();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ChooseTable != null && column != null)
            {
                if (!this.panelUserControl.Controls.Contains(ucUpdate))
                {
                    SetLabelTableName(ChooseTable);
                    this.panelUserControl.Controls.Clear();
                    this.ucUpdate.UCUpdate_SaveClick += UCUpdate_ButtonSaveClick;
                    this.panelUserControl.Controls.Add(ucUpdate);
                    this.ucUpdate.ShowData(column, dataToShow);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var str = String.Format("Are you sure to delete \"{0}\" database?", ChooseDb);
            var isArgee = MessageBox.Show(str, "Delete Table", MessageBoxButtons.YesNo);
            if(isArgee == DialogResult.Yes)
            {
                _membership.IDeleteDB(ChooseDb);
                ucTableName.Clear();
                ReadListDb_TableName();
                SetUCTableName();
            }
        }

        private void SetLabelTableName(string text)
        {
            this.labelTableName.Text = text;
            this.labelTableName.Location = new Point(
                this.labelTableName.Parent.Width / 2 - this.labelTableName.Width / 2,
                this.labelTableName.Parent.Height / 2 - this.labelTableName.Height / 2);
        }
    }
}
