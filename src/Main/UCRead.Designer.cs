﻿namespace Main
{
    partial class UCRead
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_UCRead = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UCRead)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_UCRead
            // 
            this.dataGridView_UCRead.AllowUserToDeleteRows = false;
            this.dataGridView_UCRead.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_UCRead.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridView_UCRead.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_UCRead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_UCRead.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView_UCRead.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_UCRead.MultiSelect = false;
            this.dataGridView_UCRead.Name = "dataGridView_UCRead";
            this.dataGridView_UCRead.Size = new System.Drawing.Size(549, 327);
            this.dataGridView_UCRead.TabIndex = 0;
            // 
            // UCRead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView_UCRead);
            this.Name = "UCRead";
            this.Size = new System.Drawing.Size(549, 327);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UCRead)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_UCRead;
    }
}
