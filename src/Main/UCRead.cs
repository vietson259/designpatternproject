﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class UCRead : UserControl
    {

        public UCRead()
        {
            InitializeComponent();
        }

        private Dictionary<string, DataGridViewTextBoxColumn> Props
            = new Dictionary<string, DataGridViewTextBoxColumn>();

        public void InitDataGridViewTextBoxColumn(List<string> txtColumns)
        {
            for (int i = 0; i < txtColumns.Count; i++)
            {
                try
                {
                    Props.Add(txtColumns[i], new DataGridViewTextBoxColumn()
                    {
                        HeaderText = txtColumns[i],
                        Name = txtColumns[i]
                    });
                }
                catch (System.ArgumentException) { }
            }

            foreach (string key in txtColumns)
            {
                if (!this.dataGridView_UCRead.Columns.Contains(this.Props[key]))
                {
                    this.dataGridView_UCRead.Columns.Add(Props[key]);
                }
            }
        }

        private void setLayout(DataGridView songsDataGridView, int size)
        {
            songsDataGridView.ColumnCount = size;
            songsDataGridView.Name = "songsDataGridView";
            //songsDataGridView.Location = new Point(8, 8);
            //songsDataGridView.Size = new Size(500, 250);
            songsDataGridView.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            songsDataGridView.ColumnHeadersBorderStyle =
                DataGridViewHeaderBorderStyle.Single;
            songsDataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            songsDataGridView.GridColor = Color.Black;
            songsDataGridView.RowHeadersVisible = false;
        }

        public void ShowData(List<string> column, List<List<string>> dataToShow)
        {
            setLayout(dataGridView_UCRead, column.Count);
            if (column != null && dataToShow != null)
            {
                this.dataGridView_UCRead.Columns.Clear();
                this.dataGridView_UCRead.AllowUserToAddRows = true;
                this.dataGridView_UCRead.ReadOnly = false;
                InitDataGridViewTextBoxColumn(column);
                foreach (var listData in dataToShow)
                {
                    DataGridViewRow row = (DataGridViewRow)this.dataGridView_UCRead.Rows[0].Clone();
                    for (int i = 0; i < listData.Count; i++)
                    {
                        row.Cells[i].Value = listData[i];
                    }
                    this.dataGridView_UCRead.Rows.Add(row);
                }

                this.dataGridView_UCRead.AllowUserToAddRows = false;
                this.dataGridView_UCRead.ReadOnly = true;
                dataGridView_UCRead.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.ColumnHeader);
            }
        }

        public void Is_ReadOnly(bool _readonly) => this.dataGridView_UCRead.ReadOnly = _readonly;
        public void Is_AllowToAddRow(bool _allow) => this.dataGridView_UCRead.AllowUserToAddRows = _allow;
    }
}
