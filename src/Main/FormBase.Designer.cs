using System.Collections.Generic;
using System.Windows.Forms;

namespace Main
{
    partial class FormBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelListTableName = new System.Windows.Forms.Panel();
            this.lbListTableName = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelView = new System.Windows.Forms.Panel();
            this.panelUserControl = new System.Windows.Forms.Panel();
            this.panelTableName = new System.Windows.Forms.Panel();
            this.labelTableName = new System.Windows.Forms.Label();
            this.panelMethod = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelListTableName.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelView.SuspendLayout();
            this.panelTableName.SuspendLayout();
            this.panelMethod.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelListTableName
            // 
            this.panelListTableName.AutoScroll = true;
            this.panelListTableName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelListTableName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelListTableName.Controls.Add(this.lbListTableName);
            this.panelListTableName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelListTableName.Location = new System.Drawing.Point(0, 0);
            this.panelListTableName.Name = "panelListTableName";
            this.panelListTableName.Size = new System.Drawing.Size(204, 450);
            this.panelListTableName.TabIndex = 0;
            // 
            // lbListTableName
            // 
            this.lbListTableName.AutoSize = true;
            this.lbListTableName.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbListTableName.Location = new System.Drawing.Point(38, 18);
            this.lbListTableName.Name = "lbListTableName";
            this.lbListTableName.Size = new System.Drawing.Size(132, 30);
            this.lbListTableName.TabIndex = 0;
            this.lbListTableName.Text = "Tables Name";
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelView);
            this.panelRight.Controls.Add(this.panelMethod);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(208, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(555, 450);
            this.panelRight.TabIndex = 1;
            // 
            // panelView
            // 
            this.panelView.Controls.Add(this.panelUserControl);
            this.panelView.Controls.Add(this.panelTableName);
            this.panelView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelView.Location = new System.Drawing.Point(0, 77);
            this.panelView.Name = "panelView";
            this.panelView.Size = new System.Drawing.Size(555, 373);
            this.panelView.TabIndex = 1;
            // 
            // panelUserControl
            // 
            this.panelUserControl.AutoSize = true;
            this.panelUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserControl.Location = new System.Drawing.Point(0, 38);
            this.panelUserControl.Name = "panelUserControl";
            this.panelUserControl.Size = new System.Drawing.Size(555, 335);
            this.panelUserControl.TabIndex = 5;
            // 
            // panelTableName
            // 
            this.panelTableName.Controls.Add(this.labelTableName);
            this.panelTableName.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTableName.Location = new System.Drawing.Point(0, 0);
            this.panelTableName.Name = "panelTableName";
            this.panelTableName.Size = new System.Drawing.Size(555, 38);
            this.panelTableName.TabIndex = 4;
            // 
            // labelTableName
            // 
            this.labelTableName.AutoSize = true;
            this.labelTableName.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTableName.Location = new System.Drawing.Point(231, 3);
            this.labelTableName.Name = "labelTableName";
            this.labelTableName.Size = new System.Drawing.Size(0, 32);
            this.labelTableName.TabIndex = 3;
            // 
            // panelMethod
            // 
            this.panelMethod.Controls.Add(this.btnDelete);
            this.panelMethod.Controls.Add(this.btnUpdate);
            this.panelMethod.Controls.Add(this.btnRead);
            this.panelMethod.Controls.Add(this.btnCreate);
            this.panelMethod.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMethod.Location = new System.Drawing.Point(0, 0);
            this.panelMethod.Name = "panelMethod";
            this.panelMethod.Size = new System.Drawing.Size(555, 77);
            this.panelMethod.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(426, 19);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(110, 39);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(292, 19);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(110, 39);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnRead
            // 
            this.btnRead.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRead.Location = new System.Drawing.Point(158, 19);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(110, 39);
            this.btnRead.TabIndex = 1;
            this.btnRead.Text = "Read";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.Location = new System.Drawing.Point(24, 19);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(110, 39);
            this.btnCreate.TabIndex = 0;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 100;
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelListTableName);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(204, 450);
            this.panelLeft.TabIndex = 2;
            // 
            // FormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 450);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Name = "FormBase";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormBase_Load);
            this.panelListTableName.ResumeLayout(false);
            this.panelListTableName.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelView.ResumeLayout(false);
            this.panelView.PerformLayout();
            this.panelTableName.ResumeLayout(false);
            this.panelTableName.PerformLayout();
            this.panelMethod.ResumeLayout(false);
            this.panelLeft.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelRight;
        public System.Windows.Forms.Panel panelListTableName;
        private System.Windows.Forms.Label lbListTableName;

        private ColumnHeader columnHeader1;
        private Panel panelView;
        private Panel panelMethod;
        private Button btnDelete;
        private Button btnUpdate;
        private Button btnRead;
        private Button btnCreate;
        private Panel panelLeft;
        private Panel panelTableName;
        private Label labelTableName;
        private Panel panelUserControl;
        private UCRead ucRead = new UCRead();
        private UCUpdate ucUpdate = new UCUpdate();
        private UserControlCreate ucCreate = new UserControlCreate();
        private UCTableName ucTableName = new UCTableName();
    }
}

