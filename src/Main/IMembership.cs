﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    //cac method cua interface co the them bot de phu hop voi do an
    public interface IMembership
    {
        void ICreated(string json);
        string IRead(string dbName, string tableName);
        void IUpdated(string json);
        void IDeleted(string json);
        string IConnect(string dbName);
        void ILoadData();
        string IDeleteTable(string dbName, string tableName);
        string IDeleteDB(string dbName);
        string ILoadDBName();
        string ILoadTableName(string dbName);
        string IUpdateNewColumn(string dbName, string table, string column);
    }
}
