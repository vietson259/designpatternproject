﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class UCTableName : UserControl
    {
        //private IMembership _membership;
        //private List<List<string>> _listData { get; set; }

        public event TreeNodeMouseClickEventHandler NodeClick;
        public event TreeNodeMouseClickEventHandler NodeDoubleClick;

        public UCTableName()
        {
            InitializeComponent();
        }

        public void AddListDb(TreeNode n) => this.treeViewListDb.Nodes.Add(n);

        public void Clear() => this.treeViewListDb.Nodes.Clear();

        private void treeViewListDb_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (this.NodeClick != null)
                this.NodeClick(sender, e);
        }

        private void treeViewListDb_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (this.NodeDoubleClick != null)
                this.NodeDoubleClick(sender, e);
        }
    }
}
