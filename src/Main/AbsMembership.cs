﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    public abstract class AbsMembership : IMembership
    {
        //connect db and return all tables that's exist
        public abstract string IConnect(string dbName = "qlsp");

        public abstract void ICreated(string json);

        public abstract void IDeleted(string json);

        public abstract void ILoadData();

        public abstract void IUpdated(string json);

        public abstract string ILoadTableName(string dbName);

        public abstract string ILoadDBName();

        public abstract string IRead(string dbName, string tableName);

        public abstract string IDeleteTable(string dbName, string tableName);

        public abstract string IDeleteDB(string dbName);

        public abstract string IUpdateNewColumn(string dbName, string table, string column);
    }
}
