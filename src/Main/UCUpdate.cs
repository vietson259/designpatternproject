﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Newtonsoft.Json.Linq;

namespace Main
{
    public partial class UCUpdate : UserControl
    {
        public event EventHandler UCUpdate_SaveClick;
        protected bool isChanged = false;
        protected int rowChoose = -1;
        protected int columnChoose = -1;

        //tra ve du lieu update kieu Dictionary <string, List<string>>
        //protected Dictionary<string, List<string>> data = new Dictionary<string, List<string>>(); 
        //public Dictionary<string, List<string>> Data{ get => data; set => data = value; }

        protected string jsonData = null; //tra ve json du lieu update
        public string JSONData { get => jsonData; }
        
        public UCUpdate()
        {
            InitializeComponent();
        }

        private void UCUpdate_Load(object sender, EventArgs e)
        {

        }

        private Dictionary<string, DataGridViewTextBoxColumn> Props
           = new Dictionary<string, DataGridViewTextBoxColumn>();
        public void InitDataGridViewTextBoxColumn(List<string> txtColumns)
        {
            for (int i = 0; i < txtColumns.Count; i++)
            {
                try
                {
                    Props.Add(txtColumns[i], new DataGridViewTextBoxColumn()
                    {
                        HeaderText = txtColumns[i],
                        Name = txtColumns[i]
                    });
                }
                catch (System.ArgumentException) { }
            }

            foreach (string key in txtColumns)
            {
                if (!this.dataGridView_UCUpdate.Columns.Contains(this.Props[key]))
                {
                    this.dataGridView_UCUpdate.Columns.Add(Props[key]);
                }
            }
        }

        public void ShowData(List<string> column, List<List<string>> dataToShow)
        {
            if (column != null && dataToShow != null)
            {
                this.dataGridView_UCUpdate.Columns.Clear();
                this.dataGridView_UCUpdate.AllowUserToAddRows = true;
                this.dataGridView_UCUpdate.ReadOnly = false;
                InitDataGridViewTextBoxColumn(column);
                foreach (var listData in dataToShow)
                {
                    DataGridViewRow row = (DataGridViewRow)this.dataGridView_UCUpdate.Rows[0].Clone();
                    for (int i = 0; i < listData.Count; i++)
                    {
                        row.Cells[i].Value = listData[i];
                    }
                    this.dataGridView_UCUpdate.Rows.Add(row);
                }
            }
        }

        private void dataGridView_UCUpdate_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string newName = Interaction.InputBox("Enter the new name!", "Change Column Name", "", 450, 250);
            if(newName != "")
            {
                ((DataGridView)sender).Columns[e.ColumnIndex].HeaderText = newName;
                isChanged = true;
            }
        }

        private void bntNewCol_Click(object sender, EventArgs e)
        {
            InitDataGridViewTextBoxColumn(new List<string> { "NewColumn" });
            isChanged = true;
        }

        private void dataGridView_UCUpdate_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            isChanged = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isChanged)
            {
                jsonData = GetDataInGridView();

                if (this.UCUpdate_SaveClick != null)
                    this.UCUpdate_SaveClick(sender, e);
            }
        }

        private void dataGridView_UCUpdate_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowChoose = e.RowIndex;
            columnChoose = e.ColumnIndex;
        }

        private void btnDeleteRow_Click(object sender, EventArgs e)
        {
            if (rowChoose != -1)
            {
                var isArgee = MessageBox.Show("Are you sure to delete this row?", "Delete Row",MessageBoxButtons.YesNo);
                if (isArgee == DialogResult.Yes)
                {
                    isChanged = true;
                    this.dataGridView_UCUpdate.Rows.RemoveAt(rowChoose);
                    string result = GetDataInGridView();
                    MessageBox.Show(result);
                }
            }
        }

        private void btnDeleteColumn_Click(object sender, EventArgs e)
        {
            if (columnChoose != -1)
            {
                var isArgee = MessageBox.Show("Are you sure to delete this column?", "Delete Row", MessageBoxButtons.YesNo);
                if (isArgee == DialogResult.Yes)
                {
                    isChanged = true;
                    this.dataGridView_UCUpdate.Columns.RemoveAt(columnChoose);
                    string result = GetDataInGridView();
                    MessageBox.Show(result);
                }
            }
        }

        private string GetDataInGridView()
        {
            List<JArray> value = new List<JArray>();
            JObject table = new JObject();

            foreach (DataGridViewRow row in this.dataGridView_UCUpdate.Rows)
            {
                if (row.Index != -1)
                {
                    //foreach (DataGridViewColumn col in this.dataGridView_UCUpdate.Columns)
                    //{
                    //    Data.Add(col.HeaderText, new List<string>());
                    //}
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (row.Index == 0)
                        {
                            value.Add(new JArray());
                        }
                        if (cell.Value != null)
                        {
                            //Data[cell.OwningColumn.HeaderText].Add(cell.Value.ToString());
                            value[cell.OwningColumn.Index].Add(cell.Value.ToString());
                        }
                        else
                            //Data[cell.OwningColumn.HeaderText].Add("");
                            value[cell.OwningColumn.Index].Add("");
                    }
                }
            }
            foreach (DataGridViewColumn col in this.dataGridView_UCUpdate.Columns)
            {
                table[col.HeaderText] = value[col.Index];
            }
            return table.ToString();
        }
    }
}
