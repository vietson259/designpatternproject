﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace Main
{
    public partial class UserControlCreate : UserControl
    {
        private string dbName = "";
        private TreeNode dbNode = null;
        private object currentNode = null;
        private bool modFlag = false;
        protected string dataToSave = null;

        public event EventHandler UCCreate_DoneClick;

        public UserControlCreate()
        {
            InitializeComponent();
        }

        public string DataToSave { get => dataToSave; }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!modFlag)
            {
                if (txtDB.Text != "" && txtDB.ReadOnly == false)
                {
                    dbName = txtDB.Text;
                    txtDB.ReadOnly = true;
                    txtTable.ReadOnly = false;

                    // Create the root node.
                    dbNode = new TreeNode(dbName);

                    // Add the root nodes to the TreeView.
                    tvDB.Nodes.Add(dbNode);
                }

                if (txtTable.ReadOnly == false && txtTable.Text != "")
                {
                    dbNode.Nodes.Add(txtTable.Text);
                }
            }
            else
            {
                if (currentNode == tvDB.Nodes[0])
                {
                    ((TreeNode)currentNode).Text = txtDB.Text;
                    txtDB.ReadOnly = true;
                    txtTable.ReadOnly = !txtDB.ReadOnly;
                }
                else
                {
                    ((TreeNode)currentNode).Text = txtTable.Text;
                }
                modFlag = false;
            }
            txtTable.Text = "";
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            tvDB.Nodes[0].Nodes.Remove((TreeNode)currentNode);
            if (tvDB.Nodes.Count == 0)
            {
                txtDB.ReadOnly = false;
                txtTable.ReadOnly = !txtDB.ReadOnly;
                txtTable.Text = txtDB.Text = "";
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (currentNode != null)
            {
                modFlag = true;
                if (currentNode == tvDB.Nodes[0])
                {
                    txtDB.ReadOnly = false;
                    txtTable.ReadOnly = !txtDB.ReadOnly;
                }
                else
                {
                    txtTable.Text = ((TreeNode)currentNode).Text;
                    txtDB.ReadOnly = true;
                    txtTable.ReadOnly = !txtDB.ReadOnly;
                }
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            //JArray tables = new JArray();
            //JObject db = new JObject();
            JSONObject jSONObjectArray = new JSONObject();
            jSONObjectArray.Data.Add("database", tvDB.Nodes[0].Text);
            int i = 0;
            foreach (TreeNode tn in this.tvDB.Nodes[0].Nodes)
            {
                jSONObjectArray.Data.Add(i.ToString(), tn.Text);
                i++;
            }
            dataToSave = jSONObjectArray.ToJSON();

            if (this.UCCreate_DoneClick != null)
                this.UCCreate_DoneClick(sender, e);
        }

        private void tvDB_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            currentNode = e.Node;
        }
    }
}
