﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_01_18_FormDemo
{
    public class DIContainer
    {
        private static readonly Dictionary<Type, object> RegisterModules
            = new Dictionary<Type, object>();
        public static void SetModule<TInterface, TModule>()
        {
            SetModule(typeof(TInterface), typeof(TModule));
        }
        public static T GetModule<T>()
        {
            return (T)GetModule(typeof(T));
        }
        private static void SetModule(Type interfaceType, Type moduleType)
        {
            if (!interfaceType.IsAssignableFrom(moduleType))
            {
                throw new Exception("Wrong module type");
            }
            var firstContructor = moduleType.GetConstructors()[0];
            object module = null;
            if (!firstContructor.GetParameters().Any())
            {
                module = firstContructor.Invoke(null);
            }
            else
            {
                var constructorParam = firstContructor.GetParameters();
                var moduleDependencies = new List<object>();
                foreach (var param in constructorParam)
                {
                    var dependency = GetModule(param.ParameterType);
                    moduleDependencies.Add(dependency);
                }
                module = firstContructor.Invoke(moduleDependencies.ToArray());
            }
            RegisterModules.Add(interfaceType, module);
        }
        private static object GetModule(Type interfaceType)
        {
            if (RegisterModules.ContainsKey(interfaceType))
            {
                return RegisterModules[interfaceType];
            }
            throw new Exception("Module not register");
        }
    }
}

