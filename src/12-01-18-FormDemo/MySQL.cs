﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_01_18_FormDemo
{
    public class MySQL : ABSMember
    {
        public override string IConnect(string dbName)
        {
            return Ajax.GET(API.connectDB);
        }

        public override void ICreated(string json)
        {
            //throw new NotImplementedException();
        }

        public override void IDeleted(string json)
        {
            //throw new NotImplementedException();
        }

        public override string ILoadDBName()
        {
            return Ajax.GET(API.loadAllDB);
        }

        public override void ILoadData()
        {
            //throw new NotImplementedException();
        }

        public override string ILoadTableName(string tableName)
        {
            return Ajax.POST(API.loadAllTablesInDB, @"{""content"" : """ + tableName + @"""}");
        }

        public override string IRead(string dbName, string tableName)
        {
            return Ajax.POST(API.loadDataTables, @"{""dbName"" : """ + dbName + @""", ""table"" : """ + tableName + @"""}");
        }

        public override void IUpdated(string json)
        {
            throw new NotImplementedException();
        }

        public override string IDeleteTable(string dbName, string tableName)
        {
            return Ajax.POST(API.deleteTable, @"{""dbName"" : """ + dbName + @""", ""tableName"" : """ + tableName + @"""}");
        }

        public override string IDeleteDB(string dbName)
        {
            return Ajax.POST(API.deleteDB, @"{""dbName"" : """ + dbName + @"""}");
        }

        public override string IUpdateNewColumn(string dbName, string table, string column)
        {
            return Ajax.POST(API.updateColumn, @"{""dbName"" : """ + dbName + @""", ""tableName"" : """ + table + @""", ""column"" : """ + column + @"""}");
        }
    }
}
