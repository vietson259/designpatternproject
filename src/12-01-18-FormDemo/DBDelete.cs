﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public class DBDelete : AbsDelete
    {
        public DBDelete(IMembership membership, string db) : base(membership, db)
        {

        }
        public override void Delete()
        {
            _membership.IDeleteDB(_db);
        }
    }
}
