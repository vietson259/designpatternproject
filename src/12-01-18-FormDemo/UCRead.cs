﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class UCRead : UCBase
    {
        private UCBase core = null;
        private DataGridView dataDataGridView = new DataGridView();        
        private TreeView _treeView;
        private List<string> fieldsData;
        private List<List<string>> valuesData;

        private string table = "";
        private string db = "";

        private Dictionary<string, string> valueUpdate
            = new Dictionary<string, string>();

        private Dictionary<string, AbsDelete> classDelete
            = new Dictionary<string, AbsDelete>();

        private void InitClassDelete()
        {
            classDelete.Add("rowDelete", new RowDelete(_membership, ""));
            classDelete.Add("dbDelete", new DBDelete(_membership, ""));
            classDelete.Add("tableDelete", new TableDelete(_membership, ""));
        }

        private List<Point> cellChanged = new List<Point>();

        private Dictionary<int, List<String>> listRowChanged = new Dictionary<int, List<string>>();

        public UCRead(IMembership membership, TreeView treeView, string db, string table) : base(membership, db, table)
        {
            InitializeComponent();
            InitClassDelete();
            this.table = table;
            this.db = db;
            SetLayout();
            _treeView = treeView;
        }

        public UCRead(UCBase uCBase, TreeView treeView, IMembership membership, string db, string table) : base(membership, db, table)
        {
            InitializeComponent();
            InitClassDelete();
            SetLayout();
            core = uCBase;
            PNRead.Controls.Add(uCBase);
            //SetHandlerEventUCDelete();
            this.table = table;
            this.db = db;
            _treeView = treeView;
            if (uCBase.GetType() == typeof(UCDelete))
            {
                SetHandlerEventUCDelete();
            }
            else
            {
                SetHandlerEventUCUpdate();
            }
        }

        public void SetHandlerEventUCDelete()
        {
            ((UCDelete)core).btnDeleteRow_Click += HandlerBtnDeleteRowClick;
            ((UCDelete)core).btnTableDeleteRow_Click += HandlerBtnTableDeleteClick;
            ((UCDelete)core).btnDBDeleteRow_Click += HandlerBtnDBDeleteClick;
        }

        public void SetHandlerEventUCUpdate()
        {
            ((UCUpdate)core).btnSaveChange_Click += HandlerBtnSaveChangeClick;
        }

        private void SetLayout()
        {
            dataDataGridView.Name = "dataDataGridView";
            dataDataGridView.Size = new Size(500, 250);
            dataDataGridView.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            dataDataGridView.ColumnHeadersBorderStyle =
                DataGridViewHeaderBorderStyle.Single;
            dataDataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataDataGridView.GridColor = Color.Black;
            dataDataGridView.RowHeadersVisible = true;
            dataDataGridView.MultiSelect = true;
            dataDataGridView.TabIndex = 0;

            dataDataGridView.CellClick += DataGridView_CellClick;
            dataDataGridView.CellValueChanged += DataGridView_CellValueChanged;
            //dataDataGridView.RowHeaderMouseClick += DataGridView_RowClick;
        }

        public void SetLayoutForRead()
        {
            dataDataGridView.ReadOnly = true;
        }

        public void SetLayoutForUpdate()
        {
            dataDataGridView.ReadOnly = false;
        }

        private void ShowData(List<string> header, List<List<string>> data)
        {
            dataDataGridView.ColumnCount = header.Count;
            int i = 0;
            foreach (string item in header)
            {
                dataDataGridView.Columns[i].Name = item;
                i++;
            }

            i = 0;
            foreach (List<string> item in data)
            {
                dataDataGridView.Rows.Add(item.ToArray());
            }
        }

        public void ReadData(string db, string table)
        {
            PNCore.Controls.Add(dataDataGridView);
            JSONObjectArray jsonObjectArray =
                new JSONObjectArray(_membership.IRead(db, table));
            fieldsData = (jsonObjectArray.ToJSONSimple()).Fields;
            valuesData = (jsonObjectArray.ToJSONSimple()).Values;
            ShowData(fieldsData, valuesData);
        }

        private void DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            DataGridViewRow rows = dataDataGridView.Rows[e.RowIndex];
            classDelete["rowDelete"].SetValue((string)rows.Cells[0].Value);
        }

        private void DataGridView_CellValueChanged (object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show("CellValueChanedClick");
            cellChanged.Add(new Point(e.RowIndex, e.ColumnIndex));
            if (!valueUpdate.ContainsKey(dataDataGridView.Columns[e.ColumnIndex].HeaderText))
            {
                valueUpdate.Add(
                    dataDataGridView.Columns[e.ColumnIndex].HeaderText,
                    dataDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString());
            }
        }

        private void HandlerBtnDeleteRowClick(object sender, EventArgs e)
        {
            //classDelete["rowDelete"].SetValue()
            classDelete["rowDelete"].SetDB(_db);
            classDelete["rowDelete"].SetTable(_table);
            classDelete["rowDelete"].Delete();
        }

        private void HandlerBtnTableDeleteClick(object sender, EventArgs e)
        {
            classDelete["tableDelete"].SetTable(_table);
            classDelete["tableDelete"].Delete();
        }

        private void HandlerBtnDBDeleteClick(object sender, EventArgs e)
        {
            classDelete["dbDelete"].SetDB(_db);
            classDelete["dbDelete"].Delete();
        }

        private void DataGridView_RowClick(object sender, EventArgs e)
        {
            //MessageBox.Show(((DataGridViewCellMouseEventArgs)e).RowIndex.ToString());
        }

        private void HandlerBtnSaveChangeClick(object  sender, EventArgs e)
        {
            JSONObject jSONObject = new JSONObject();
            jSONObject.Data.Add("database", db);
            jSONObject.Data.Add("table", table);
            foreach (string key in valueUpdate.Keys)
            {
                jSONObject.Data.Add(key, valueUpdate[key]);
            }

            Ajax.POST(API.updateRow, jSONObject.ToJSON());
        }
    }
}
