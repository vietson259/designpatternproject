﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_01_18_FormDemo
{
    public abstract class ABSMember : IMembership
    {
        public abstract string IConnect(string dbName);

        public abstract void ICreated(string json);

        public abstract void IDeleted(string json);

        public abstract string IDeleteDB(string dbName);

        public abstract string IDeleteTable(string dbName, string tableName);

        public abstract void ILoadData();

        public abstract string ILoadDBName();

        public abstract string ILoadTableName(string dbName);

        public abstract string IRead(string dbName, string tableName);

        public abstract void IUpdated(string json);

        public abstract string IUpdateNewColumn(string dbName, string table, string column);
    }
}
