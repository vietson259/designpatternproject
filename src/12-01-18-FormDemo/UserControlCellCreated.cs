﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class UserControlCellCreated : UserControl
    {
        public UserControlCellCreated()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DestroyHandle();
            UserControlCreated.ReSetLocationCellCreated();
        }
    }
}
