﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_01_18_FormDemo
{
    public class ServerMembership 
    {
        private AbForm _abForm;
        public ServerMembership()
        {
            _abForm = new AbForm();
        }
        public void Created_()
        {
            _abForm.Show();
        }
        public void Deleted()
        {
            
        }
        public void Read()
        {
            
        }
        public void Updated()
        {
            
        }
    }
}
