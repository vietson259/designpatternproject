﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class UCCreate : UCBase
    {
        private string dbName = "";
        private TreeNode dbNode = null;
        private TreeNode currentNode = null;
        protected string dataToSave = null;
        private bool modFlag = false;

        Dictionary<string, int> tables = new Dictionary<string, int>();
        Dictionary<string, Dictionary<string, int>> columns
            = new Dictionary<string, Dictionary<string, int>>();

        public UCCreate()
        {
            InitializeComponent();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (dbName == "" || (txtDB.Text != dbName && txtDB.Text != ""))
            {
                dbName = txtDB.Text;
                dbNode = new TreeNode(dbName);
                tvDB.Nodes.Add(dbNode);
            }
            if (txtTable.Text != "")
            {
                if (!tables.ContainsKey(txtTable.Text))
                {
                    tables[txtTable.Text] = tables.Count;
                    columns.Add(txtTable.Text, new Dictionary<string, int>());
                    tvDB.Nodes[0].Nodes.Add(txtTable.Text);
                }
            }
            if (txtColumn.Text != "" && !columns[txtTable.Text].ContainsKey(txtColumn.Text))
            {
                int i = tables[txtTable.Text];
                tvDB.Nodes[0].Nodes[i].Nodes.Add(txtColumn.Text);
                columns[txtTable.Text].Add(txtColumn.Text, columns[txtTable.Text].Count);
            }
            txtColumn.Text = "";
        }

        private void BtnMod_Click(object sender, EventArgs e)
        {
            if (currentNode != null && !modFlag)
            {
                modFlag = true;
                if (currentNode == tvDB.Nodes[0])
                {
                    txtDB.ReadOnly = false;
                    txtTable.ReadOnly = !txtDB.ReadOnly;
                }
                else
                {
                    txtTable.Text = currentNode.Text;
                    txtDB.ReadOnly = true;
                    txtTable.ReadOnly = !txtDB.ReadOnly;
                }
            } else if (modFlag)
            {
                if (currentNode.Level == 0)
                {
                    dbName = currentNode.Text = txtDB.Text;
                } else if (currentNode.Level == 1)
                {
                    string temp = currentNode.Text;
                    currentNode.Text = txtTable.Text;
                    tables.Add(currentNode.Text, tables[temp]);
                    tables.Remove(temp);

                    Dictionary<string, int> tempColumn = columns[temp];
                    columns.Add(currentNode.Text, tempColumn);
                    columns.Remove(currentNode.Text);
                } else
                {
                    currentNode.Text = txtColumn.Text;
                }
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            tvDB.Nodes[0].Nodes.Remove((TreeNode)currentNode);
            if (tvDB.Nodes.Count == 0)
            {
                txtDB.ReadOnly = false;
                txtTable.ReadOnly = !txtDB.ReadOnly;
                txtTable.Text = txtDB.Text = "";
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObjectColumn = new JSONObject();
            jSONObject.Data.Add("database", tvDB.Nodes[0].Text);
            int i = 0;
            foreach (TreeNode table in tvDB.Nodes[0].Nodes)
            {
                foreach (TreeNode tn in tvDB.Nodes[0].Nodes[i].Nodes)
                {
                    jSONObjectColumn.Data.Add(tn.Text, true.ToString());
                }
                jSONObject.Data.Add(tvDB.Nodes[0].Nodes[i].Text, jSONObjectColumn.ToJSON());
                i++;
                jSONObjectColumn.Data.Clear();
            }
            dataToSave = jSONObject.ToJSON();
            Ajax.POST(API.createDataTable, dataToSave);
        }

        public string DataToSave { get => dataToSave; }

        private void tvDB_NodeMouseClick_1(object sender, TreeNodeMouseClickEventArgs e)
        {
            currentNode = e.Node;
        }
    }
}
