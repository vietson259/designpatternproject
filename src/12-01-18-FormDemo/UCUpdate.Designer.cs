﻿namespace _12_01_18_FormDemo
{
    partial class UCUpdate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnSaveChange = new System.Windows.Forms.Button();
            this.PNCore.SuspendLayout();
            this.SuspendLayout();
            // 
            // PNCore
            // 
            this.PNCore.Controls.Add(this.BtnSaveChange);
            // 
            // BtnSaveChange
            // 
            this.BtnSaveChange.Location = new System.Drawing.Point(188, 16);
            this.BtnSaveChange.Name = "BtnSaveChange";
            this.BtnSaveChange.Size = new System.Drawing.Size(110, 41);
            this.BtnSaveChange.TabIndex = 3;
            this.BtnSaveChange.Text = "Save Data";
            this.BtnSaveChange.UseVisualStyleBackColor = true;
            this.BtnSaveChange.Click += new System.EventHandler(this.BtnSaveChange_Click);
            // 
            // UCUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "UCUpdate";
            this.PNCore.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BtnSaveChange;
    }
}
