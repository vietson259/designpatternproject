﻿namespace _12_01_18_FormDemo
{
    partial class UserControlCreated
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbTableName = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.BtnAddColumn = new System.Windows.Forms.Button();
            this.PnListCell = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // LbTableName
            // 
            this.LbTableName.AutoSize = true;
            this.LbTableName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTableName.Location = new System.Drawing.Point(28, 32);
            this.LbTableName.Name = "LbTableName";
            this.LbTableName.Size = new System.Drawing.Size(91, 21);
            this.LbTableName.TabIndex = 0;
            this.LbTableName.Text = "Table Name";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(137, 29);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(245, 29);
            this.textBox1.TabIndex = 1;
            // 
            // BtnAddColumn
            // 
            this.BtnAddColumn.Location = new System.Drawing.Point(32, 87);
            this.BtnAddColumn.Name = "BtnAddColumn";
            this.BtnAddColumn.Size = new System.Drawing.Size(75, 23);
            this.BtnAddColumn.TabIndex = 3;
            this.BtnAddColumn.Text = "Add";
            this.BtnAddColumn.UseVisualStyleBackColor = true;
            this.BtnAddColumn.Click += new System.EventHandler(this.BtnAddColumn_Click);
            // 
            // PnListCell
            // 
            this.PnListCell.AutoScroll = true;
            this.PnListCell.Location = new System.Drawing.Point(3, 116);
            this.PnListCell.Name = "PnListCell";
            this.PnListCell.Size = new System.Drawing.Size(466, 218);
            this.PnListCell.TabIndex = 4;
            // 
            // UserControlCreated
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.PnListCell);
            this.Controls.Add(this.BtnAddColumn);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.LbTableName);
            this.Name = "UserControlCreated";
            this.Size = new System.Drawing.Size(472, 337);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbTableName;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button BtnAddColumn;
        private System.Windows.Forms.Panel PnListCell;
    }
}
