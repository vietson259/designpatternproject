﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public class RowDelete : AbsDelete
    {
        public string db = "";
        public string table = "";
        public RowDelete(IMembership membership, string rowIndex) : base(membership, rowIndex)
        {

        }
        public override void Delete()
        {
            Ajax.POST(API.deleteRow, @"{""database"" : """ + _db + @""", ""table"" : """ + _table + @""", ""key"" : """ + _value + @"""}");
        }
    }
}
