﻿namespace _12_01_18_FormDemo
{
    partial class UCCreate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTable1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtColumn = new System.Windows.Forms.TextBox();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.BtnMod = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.tvDB = new System.Windows.Forms.TreeView();
            this.txtTable = new System.Windows.Forms.TextBox();
            this.txtDB = new System.Windows.Forms.TextBox();
            this.PNCore.SuspendLayout();
            this.SuspendLayout();
            // 
            // PNCore
            // 
            this.PNCore.Controls.Add(this.tvDB);
            this.PNCore.Controls.Add(this.BtnSave);
            this.PNCore.Controls.Add(this.BtnDelete);
            this.PNCore.Controls.Add(this.BtnMod);
            this.PNCore.Controls.Add(this.BtnAdd);
            this.PNCore.Controls.Add(this.txtColumn);
            this.PNCore.Controls.Add(this.label3);
            this.PNCore.Controls.Add(this.txtDB);
            this.PNCore.Controls.Add(this.txtTable);
            this.PNCore.Controls.Add(this.TxtTable1);
            this.PNCore.Controls.Add(this.label1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Database";
            // 
            // TxtTable1
            // 
            this.TxtTable1.AutoSize = true;
            this.TxtTable1.Location = new System.Drawing.Point(31, 72);
            this.TxtTable1.Name = "TxtTable1";
            this.TxtTable1.Size = new System.Drawing.Size(34, 13);
            this.TxtTable1.TabIndex = 0;
            this.TxtTable1.Text = "Table";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Column";
            // 
            // txtColumn
            // 
            this.txtColumn.Location = new System.Drawing.Point(100, 106);
            this.txtColumn.Name = "txtColumn";
            this.txtColumn.Size = new System.Drawing.Size(214, 20);
            this.txtColumn.TabIndex = 1;
            // 
            // BtnAdd
            // 
            this.BtnAdd.Location = new System.Drawing.Point(358, 30);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(116, 36);
            this.BtnAdd.TabIndex = 2;
            this.BtnAdd.Text = "Add";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // BtnMod
            // 
            this.BtnMod.Location = new System.Drawing.Point(358, 90);
            this.BtnMod.Name = "BtnMod";
            this.BtnMod.Size = new System.Drawing.Size(116, 36);
            this.BtnMod.TabIndex = 2;
            this.BtnMod.Text = "Modify";
            this.BtnMod.UseVisualStyleBackColor = true;
            this.BtnMod.Click += new System.EventHandler(this.BtnMod_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(358, 151);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(116, 36);
            this.BtnDelete.TabIndex = 2;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(358, 213);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(116, 36);
            this.BtnSave.TabIndex = 2;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // tvDB
            // 
            this.tvDB.Location = new System.Drawing.Point(102, 148);
            this.tvDB.Name = "tvDB";
            this.tvDB.Size = new System.Drawing.Size(211, 172);
            this.tvDB.TabIndex = 3;
            this.tvDB.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvDB_NodeMouseClick_1);
            // 
            // txtTable
            // 
            this.txtTable.Location = new System.Drawing.Point(100, 69);
            this.txtTable.Name = "txtTable";
            this.txtTable.Size = new System.Drawing.Size(214, 20);
            this.txtTable.TabIndex = 1;
            // 
            // txtDB
            // 
            this.txtDB.Location = new System.Drawing.Point(100, 30);
            this.txtDB.Name = "txtDB";
            this.txtDB.Size = new System.Drawing.Size(214, 20);
            this.txtDB.TabIndex = 1;
            // 
            // UCCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "UCCreate";
            this.PNCore.ResumeLayout(false);
            this.PNCore.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView tvDB;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnMod;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.TextBox txtColumn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label TxtTable1;
        private System.Windows.Forms.TextBox txtTable;
        private System.Windows.Forms.TextBox txtDB;
    }
}
