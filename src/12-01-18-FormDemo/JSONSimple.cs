﻿using System.Collections.Generic;

namespace _12_01_18_FormDemo
{
    public class JSONSimple
    {
        private List<string> _fields;
        private List<List<string>> _values;
        public List<string> Fields { get => _fields; set => _fields = value; }
        public List<List<string>> Values { get => _values; set => _values = value; }
    }
}