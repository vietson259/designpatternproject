﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_01_18_FormDemo
{
    public static class ConfigDIContainer
    {
        public static void Config()
        {
            DIContainer.SetModule<IMembership, MySQL>();
        }
    }
}
