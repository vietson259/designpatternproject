﻿namespace _12_01_18_FormDemo
{
    partial class UCRead
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PNRead = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // PNCore
            // 
            this.PNCore.Size = new System.Drawing.Size(516, 268);
            // 
            // PNRead
            // 
            this.PNRead.Location = new System.Drawing.Point(0, 269);
            this.PNRead.Name = "PNRead";
            this.PNRead.Size = new System.Drawing.Size(518, 80);
            this.PNRead.TabIndex = 2;
            // 
            // UCRead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PNRead);
            this.Name = "UCRead";
            this.Size = new System.Drawing.Size(519, 350);
            this.Controls.SetChildIndex(this.PNCore, 0);
            this.Controls.SetChildIndex(this.PNRead, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PNRead;
    }
}
