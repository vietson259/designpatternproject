﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_01_18_FormDemo
{
    public abstract class AbsDelete
    {
        protected string _value;
        protected string _db;
        protected string _table;
        protected IMembership _membership;
        public AbsDelete(IMembership membership, string value)
        {
            _membership = membership;
            _value = value;
        }
        public abstract void Delete();
        public void SetValue(string value)
        {
            _value = value;
        }
        public void SetDB(string db)
        {
            _db = db;
        }
        public void SetTable(string value)
        {
            _table = value;
        }
    }
}
