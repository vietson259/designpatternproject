﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class UCBase : UserControl
    {
        public string _db = "";
        public string _table = "";
        protected IMembership _membership;
        public UCBase()
        {
            InitializeComponent();
        }
        public UCBase(IMembership membership, string db, string table)
        {
            InitializeComponent();
            _membership = membership;
            _db = db; _table = table;
        }
        public UCBase(IMembership membership)
        {
            InitializeComponent();
            _membership = membership;
        }
    }
}
