﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class UserControlCreated : UserControl
    {
        private static List<UserControlCellCreated> userControlCellCreatedsList
            = new List<UserControlCellCreated>();
        public static void ReSetLocationCellCreated()
        {
            int y = 0;
            foreach (UserControlCellCreated uc in userControlCellCreatedsList)
            {
                uc.Location = new Point(
                     0, y
                );
                y += 50;
            }
        }
        public UserControlCreated()
        {
            InitializeComponent();
        }

        private void BtnAddColumn_Click(object sender, EventArgs e)
        {
            UserControlCellCreated userControlCellCreated = null;
            if (userControlCellCreatedsList.Count() == 0)
            {
                userControlCellCreated
                = new UserControlCellCreated()
                {
                    Location = new Point(0, 0)
                };
                userControlCellCreatedsList.Add(userControlCellCreated);
            } else
            {
                userControlCellCreated
                = new UserControlCellCreated()
                {
                    Location = new Point(userControlCellCreatedsList.Last().Location.X,
                        (userControlCellCreatedsList.Last().Location.Y + 50))
                };
                userControlCellCreatedsList.Add(userControlCellCreated);
            }
            this.PnListCell.Controls.Add(userControlCellCreated);
        }
    }
}
