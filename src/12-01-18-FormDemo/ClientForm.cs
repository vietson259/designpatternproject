﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class ClientForm : Form
    {
        protected UCBase _uCBase;
        protected readonly IMembership _membership;
        protected List<string> listDb = new List<string>();
        Dictionary<string, List<string>> listTableName = new Dictionary<string, List<string>>();
        protected string db = "";
        protected string tableName = "";

        public ClientForm(IMembership membership)
        {
            InitializeComponent();
            _membership = membership;
            ReadListDb_TableName();
            TVDB.BeginUpdate();
            int i = 0;
            foreach (KeyValuePair<string, List<string>> item in listTableName)
            {
                TVDB.Nodes.Add(item.Key);
                foreach (string val in item.Value)
                {
                    TVDB.Nodes[i].Nodes.Add(val);
                }
                i++;
            }
            TVDB.EndUpdate();
        }

        private void AddDataToDTG()
        {
            TVDB.Nodes.Clear();
            ReadListDb_TableName();
            TVDB.BeginUpdate();
            int i = 0;
            foreach (KeyValuePair<string, List<string>> item in listTableName)
            {
                TVDB.Nodes.Add(item.Key);
                foreach (string val in item.Value)
                {
                    TVDB.Nodes[i].Nodes.Add(val);
                }
                i++;
            }
            TVDB.EndUpdate();
        }

        private void ReadListDb_TableName()
        {
            JSONObjectArray jsonObjectArray = new JSONObjectArray(_membership.ILoadDBName());
            listDb.Clear();
            listTableName.Clear();

            //Danh sach ten cac db
            foreach (List<string> element in jsonObjectArray.ToJSONSimple().Values)
            {
                listDb.Add(element[0]);
            }

            //Danh sach table trong moi db
            foreach (string dbName in listDb)
            {
                List<string> listTables = new List<string>();
                jsonObjectArray = new JSONObjectArray(_membership.ILoadTableName(dbName));
                foreach (List<string> element in jsonObjectArray.ToJSONSimple().Values)
                {
                    listTables.Add(element[0]);
                }
                listTableName.Add(dbName, listTables);
            }

            //AddDataToDTG();
        }

        private void SetUserControl(UCBase uCBase)
        {
            _uCBase = uCBase;
        }

        private void TVDB_NodeMouseClick_1(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Level != 0)
            {
                tableName = e.Node.Text;
                db = e.Node.Parent.Text;
            }
        }

        private void BtnRead_Click(object sender, EventArgs e)
        {
            SetUserControl(new UCRead(_membership, TVDB, db, tableName));
            ((UCRead)_uCBase).SetLayoutForRead();
            _uCBase._table = tableName;
            _uCBase._db = db;
            LoadData();
            AddDataToDTG();
        }

        private void LoadData()
        {
            if (db != "" && tableName != "")
            {
                PN1.Controls.Clear();
                PN1.Controls.Add(_uCBase);
                ((UCRead)_uCBase).ReadData(db, tableName);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            SetUserControl(new UCRead(new UCDelete(), TVDB, _membership, db, tableName));
            PN1.Controls.Clear();
            PN1.Controls.Add(_uCBase);
            ((UCRead)_uCBase).ReadData(db, tableName);
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            SetUserControl(new UCRead(new UCUpdate(), TVDB, _membership, db, tableName));
            PN1.Controls.Clear();
            PN1.Controls.Add(_uCBase);
            ((UCRead)_uCBase).ReadData(db, tableName);
        }

        private void BtnCreate_Click(object sender, EventArgs e)
        {
            PN1.Controls.Clear();
            _uCBase = new UCCreate();
            PN1.Controls.Add(_uCBase);
        }

        private void TVDB_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            SetUserControl(new UCRead(_membership, TVDB, db, tableName));
            ((UCRead)_uCBase).SetLayoutForRead();
            LoadData();
        }
    }
}
