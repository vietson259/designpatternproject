﻿namespace _12_01_18_FormDemo
{
    partial class UCDelete
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnDeleteRow = new System.Windows.Forms.Button();
            this.BtnTableDelete = new System.Windows.Forms.Button();
            this.BtnDBDelete = new System.Windows.Forms.Button();
            this.PNCore.SuspendLayout();
            this.SuspendLayout();
            // 
            // PNCore
            // 
            this.PNCore.Controls.Add(this.BtnDBDelete);
            this.PNCore.Controls.Add(this.BtnTableDelete);
            this.PNCore.Controls.Add(this.BtnDeleteRow);
            this.PNCore.Size = new System.Drawing.Size(497, 350);
            // 
            // BtnDeleteRow
            // 
            this.BtnDeleteRow.Location = new System.Drawing.Point(16, 16);
            this.BtnDeleteRow.Name = "BtnDeleteRow";
            this.BtnDeleteRow.Size = new System.Drawing.Size(110, 41);
            this.BtnDeleteRow.TabIndex = 0;
            this.BtnDeleteRow.Text = "Delete Row";
            this.BtnDeleteRow.UseVisualStyleBackColor = true;
            this.BtnDeleteRow.Click += new System.EventHandler(this.BtnDeleteRow_Click);
            // 
            // BtnTableDelete
            // 
            this.BtnTableDelete.Location = new System.Drawing.Point(188, 16);
            this.BtnTableDelete.Name = "BtnTableDelete";
            this.BtnTableDelete.Size = new System.Drawing.Size(110, 41);
            this.BtnTableDelete.TabIndex = 0;
            this.BtnTableDelete.Text = "Delete Table";
            this.BtnTableDelete.UseVisualStyleBackColor = true;
            this.BtnTableDelete.Click += new System.EventHandler(this.BtnTableDelete_Click);
            // 
            // BtnDBDelete
            // 
            this.BtnDBDelete.Location = new System.Drawing.Point(359, 16);
            this.BtnDBDelete.Name = "BtnDBDelete";
            this.BtnDBDelete.Size = new System.Drawing.Size(110, 41);
            this.BtnDBDelete.TabIndex = 0;
            this.BtnDBDelete.Text = "Delete Database";
            this.BtnDBDelete.UseVisualStyleBackColor = true;
            this.BtnDBDelete.Click += new System.EventHandler(this.BtnDBDelete_Click);
            // 
            // UCDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "UCDelete";
            this.PNCore.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnDeleteRow;
        private System.Windows.Forms.Button BtnDBDelete;
        private System.Windows.Forms.Button BtnTableDelete;
    }
}
