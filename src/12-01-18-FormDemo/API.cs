﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_01_18_FormDemo
{
    public static class API
    {
        public static string connectDB = "http://localhost:5000/";
        public static string loadAllDB = "http://localhost:5000/read/getAllDb";
        public static string loadAllTablesInDB = "http://localhost:5000/read/getAllTablesInDb";
        public static string loadDataTables = "http://localhost:5000/read/getDataTable";
        public static string createData = "http://localhost:5000/create/createNewDatabase";
        public static string deleteTable = "http://localhost:5000/delete/deleteTable";
        public static string deleteDB = "http://localhost:5000/delete/deleteDatabase";
        public static string updateColumn = "http://localhost:5000/update/updateNewColumn";
        public static string createDataTable = "http://localhost:5000/create/createDataTable";
        public static string updateRow = "http://localhost:5000/update/insertRow";
        public static string deleteRow = "http://localhost:5000/delete/deleteRow";
    }
}
