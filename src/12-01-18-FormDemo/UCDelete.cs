﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class UCDelete : UCBase
    {
        public event EventHandler btnDeleteRow_Click;
        public event EventHandler btnTableDeleteRow_Click;
        public event EventHandler btnDBDeleteRow_Click;
        public UCDelete()
        {
            InitializeComponent();
        }

        private void BtnDeleteRow_Click(object sender, EventArgs e)
        {
            if (this.btnDeleteRow_Click != null)
                this.btnDeleteRow_Click(sender, e);
        }

        private void BtnTableDelete_Click(object sender, EventArgs e)
        {
            if (this.btnTableDeleteRow_Click != null)
                this.btnTableDeleteRow_Click(sender, e);
        }

        private void BtnDBDelete_Click(object sender, EventArgs e)
        {
            if (this.btnDBDeleteRow_Click != null)
                this.btnDBDeleteRow_Click(sender, e);
        }
    }
}
