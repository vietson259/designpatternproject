﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class AbForm : Form
    {
        public AbForm()
        {
            InitializeComponent();
            InitDataGridViewTextBoxColumn(new string[]
            {
                "ID", "Name", "Age", "Job"
            });
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            InitDataGridViewTextBoxColumn(new string[]
            {
                "City", "Gender"
            });
        }
    }
}
