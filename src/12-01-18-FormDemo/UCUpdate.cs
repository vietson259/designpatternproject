﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _12_01_18_FormDemo
{
    public partial class UCUpdate : UCBase
    {
        public event EventHandler btnSaveChange_Click;

        public UCUpdate()
        {
            InitializeComponent();
        }

        private void BtnSaveChange_Click(object sender, EventArgs e)
        {
            if (this.btnSaveChange_Click != null)
                this.btnSaveChange_Click(sender, e);
        }
    }
}
