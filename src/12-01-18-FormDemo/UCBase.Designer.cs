﻿namespace _12_01_18_FormDemo
{
    partial class UCBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PNCore = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // PNCore
            // 
            this.PNCore.Location = new System.Drawing.Point(0, 0);
            this.PNCore.Name = "PNCore";
            this.PNCore.Size = new System.Drawing.Size(500, 350);
            this.PNCore.TabIndex = 1;
            // 
            // UCBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PNCore);
            this.Name = "UCBase";
            this.Size = new System.Drawing.Size(500, 350);
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.Panel PNCore;
    }
}
